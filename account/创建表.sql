-- 删除表 创建t_user表
DROP TABLE IF EXISTS t_user;
CREATE TABLE `t_user` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键，自增Id,PK',
  `userCode` VARCHAR(20) DEFAULT NULL COMMENT '登陆名，code',
  `userName` VARCHAR(30) DEFAULT NULL COMMENT '用户名',
  `passWord` VARCHAR(20) DEFAULT NULL COMMENT '密码',
  `sex` VARCHAR(3) DEFAULT NULL COMMENT '性别',
  `birthday` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '生日',
  `isValid` VARCHAR(2) DEFAULT NULL COMMENT '是否有效',
  `phone` VARCHAR(15) DEFAULT NULL COMMENT '联系方式',
  `insertTime` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '插入时间',
  `modifyTIme` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`Id`)
)COMMENT='用户表';

--删除表 创建T_BillTradeDeatil表
DROP TABLE IF EXISTS T_BillTradeDeatil;
CREATE TABLE T_BillTradeDeatil(
    Id    INT    NOT NULL AUTO_INCREMENT COMMENT '主键，自增Id',
    billType    VARCHAR(20) COMMENT '账单类型',
    recordDate    TIMESTAMP COMMENT '记账日期',
    recorUser    INT COMMENT '记录用户',
    payUser    INT COMMENT '支付用户',
    totalMoney    DECIMAL(8,3) COMMENT '账单总金额',
    description    VARCHAR(500) COMMENT '账单描述',
    pageUrl    VARCHAR(100) COMMENT '图片路径',
    isClean    VARCHAR(2) COMMENT '是否结清',
    MAKETIME    TIMESTAMP COMMENT '插入时间',
    modifyTIme    TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (Id)
)COMMENT='账单交易明细表';

--删除表 创建T_AvgBillDetail表
DROP TABLE IF EXISTS T_AvgBillDetail;
CREATE TABLE T_AvgBillDetail(
    Id    INT NOT NULL AUTO_INCREMENT COMMENT '主键，自增Id',
    Bill_Id    INT COMMENT '账单Id',
    User_Id    INT COMMENT '用户id',
    AvgMoney    DECIMAL(8,3) COMMENT '均摊金额',
    MAKETIME    TIMESTAMP COMMENT '插入时间',
    modifyTIme    TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (Id)
)COMMENT='账单平均均摊明细信息表';

--删除表 创建T_CodeDict表
DROP TABLE IF EXISTS T_CodeDict;
CREATE TABLE T_CodeDict(
    Id    INT  AUTO_INCREMENT  NOT NULL COMMENT '主键，自增Id',
    codeType    VARCHAR(50) COMMENT 'code的类型',
    CODE    VARCHAR(30) COMMENT 'code值',
    codeName    VARCHAR(50) COMMENT 'code名称',
    codeDes    VARCHAR(500) COMMENT 'code描述',
    PRIMARY KEY (Id)
) COMMENT='数据字典表';
