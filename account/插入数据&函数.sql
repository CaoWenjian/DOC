/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.6.36 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;
--- 用户表插入
insert into `t_user` ( `userCode`, `userName`, `passWord`, `sex`, `birthday`, `isValid`, `phone`, `insertTime`, `modifyTIme`) 
values('15102786513','操文健','001','1','2017-09-15 07:30:21','01','15102786513','2017-09-15 07:29:55','2017-09-15 07:30:21');

insert into `t_user` ( `userCode`, `userName`, `passWord`, `sex`, `birthday`, `isValid`, `phone`, `insertTime`, `modifyTIme`) 
values('15827035955','夏磊','001','1','2017-09-15 07:30:21','01','15827035955','2017-09-15 07:29:55','2017-09-15 07:30:21');

insert into `t_user` ( `userCode`, `userName`, `passWord`, `sex`, `birthday`, `isValid`, `phone`, `insertTime`, `modifyTIme`) 
values('13277954236','熊聪','001','1','2017-09-15 07:30:21','01','13277954236','2017-09-15 07:29:55','2017-09-15 07:30:21');

insert into `t_user` ( `userCode`, `userName`, `passWord`, `sex`, `birthday`, `isValid`, `phone`, `insertTime`, `modifyTIme`) 
values('15567697757','潘鸿飞','001','1','2017-09-15 07:30:21','01','15567697757','2017-09-15 07:29:55','2017-09-15 07:30:21');

insert into `t_user` ( `userCode`, `userName`, `passWord`, `sex`, `birthday`, `isValid`, `phone`, `insertTime`, `modifyTIme`) 
values('13545878146','彭文','001','1','2017-09-15 07:30:21','01','13545878146','2017-09-15 07:29:55','2017-09-15 07:30:21');


-- 字典表插入
INSERT INTO `t_codedict` (`codetype`,`code`,`codename`) 
VALUES('BOOLFLAG','00','否');
INSERT INTO `t_codedict` (`codetype`,`code`,`codename`) 
VALUES('BOOLFLAG','01','是');

INSERT INTO `t_codedict` (`codetype`,`code`,`codename`) 
VALUES('SEX','00','女');
INSERT INTO `t_codedict` (`codetype`,`code`,`codename`) 
VALUES('SEX','01','男');
INSERT INTO `t_codedict` (`codetype`,`code`,`codename`) 
VALUES('SEX','02','未知');

INSERT INTO `t_codedict` (`codetype`,`code`,`codename`) VALUES('COSTTYPE','00','买菜');
INSERT INTO `t_codedict` (`codetype`,`code`,`codename`) VALUES('COSTTYPE','01','外卖');
INSERT INTO `t_codedict` (`codetype`,`code`,`codename`) VALUES('COSTTYPE','02','聚餐吃饭');
INSERT INTO `t_codedict` (`codetype`,`code`,`codename`) VALUES('COSTTYPE','03','饮料');
INSERT INTO `t_codedict` (`codetype`,`code`,`codename`) VALUES('COSTTYPE','11','水费');
INSERT INTO `t_codedict` (`codetype`,`code`,`codename`) VALUES('COSTTYPE','12','电费');
INSERT INTO `t_codedict` (`codetype`,`code`,`codename`) VALUES('COSTTYPE','13','生活用品');
INSERT INTO `t_codedict` (`codetype`,`code`,`codename`) VALUES('COSTTYPE','99','其他');




-- getCodeName 函数创建
DELIMITER $$

USE `account`$$

DROP FUNCTION IF EXISTS `getcodename`$$

CREATE FUNCTION `getcodename`(`tcodetype` VARCHAR(100),`tcode` VARCHAR(100)) RETURNS VARCHAR(300) CHARSET utf8
BEGIN
	DECLARE result VARCHAR(300);
	
	IF tcodetype='USER'
	THEN
	    SELECT uu.username INTO result FROM t_user uu WHERE uu.id=tcode; 
	ELSE
	    SELECT CASE WHEN t.codename IS NULL THEN t.code WHEN t.codename IS NULL AND t.code IS NULL THEN "Get_No_Data"  ELSE t.codename END INTO result FROM t_codedict t WHERE t.codeType=tcodetype AND t.CODE=tcode;
	END IF;
	RETURN(result);
END$$

